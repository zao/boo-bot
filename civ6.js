var base_choices = [
    ["Catherine de Medici", "French"],
    ["Chandragupta", "Indian"],
    ["Cleopatra", "Egyptian"],
    ["Frederick Barbarossa", "German"],
    ["Gandhi", "Indian"],
    ["Gilgamesh", "Sumerian"],
    ["Gorgo", "Greek"],
    ["Harald Hardrada", "Norwegian"],
    ["Hojo Tokimune", "Japanese"],
    ["Montezuma", "Aztec"],
    ["Mvemba a Nzinga", "Kongolese"],
    ["Pedro II", "Brazilian"],
    ["Pericles", "Greek"],
    ["Peter", "Russian"],
    ["Philip II", "Spanish"],
    ["Qin Shi Huang", "Chinese"],
    ["Saladin", "Arabian"],
    ["Teddy Roosevelt", "American"],
    ["Tomyris", "Scythian"],
    ["Trajan", "Roman"],
    ["Victoria", "English"],
];

var base_dlc_choices = [
    ["Alexander", "Macedonian"],
    ["Amanitore", "Nubian"],
    ["Cyrus", "Persian"],
    ["Gitarja", "Indonesian"],
    ["Jadwiga", "Polish"],
    ["Jayavarman VII", "Khmer"],
    ["John Curtin", "Australian"],
];

var rf_choices = [
    ["Genghis Khan", "Mongolian"],
    ["Lautaro", "Mapuche"],
    ["Poundmaker", "Cree"],
    ["Robert the Bruce", "Scottish"],
    ["Seondeok", "Korean"],
    ["Shaka", "Zulu"],
    ["Tamar", "Georgian"],
    ["Wilhelmina", "Dutch"],
];

var gs_choices = [
    ["Dido", "Phoenician"],
    ["Eleanor of Aquitaine", "English"],
    ["Eleanor of Aquitaine", "French"],
    ["Kristina", "Swedish"],
    ["Kupe", "Māori"],
    ["Mansa Musa", "Malian"],
    ["Matthias Corvinus", "Hungarian"],
    ["Pachacuti", "Incan"],
    ["Suleiman", "Ottoman"],
    ["Wilfrid Laurier", "Canadian"],
];

var all_choices = {};
var all_nations = new Set();
var all_leaders = new Set();

var populate_group = (choices) => {
    choices.forEach(choice => {
        let leader = choice[0];
        let nation = choice[1];
        let nations = all_choices[leader];
        if (!nations) {
            nations = [nation];
        }
        else {
            nations.push(nation);
        }
        all_choices[leader] = nations;
        all_leaders.add(leader);
        all_nations.add(nation);
    });
};

populate_group(base_choices);
populate_group(base_dlc_choices);
populate_group(rf_choices);
populate_group(gs_choices);

function draft_gs_round(nCivs, players) {
    var player_count = players.length;
    var selection_count = nCivs;

    var pool = [];
    Object.keys(all_choices).forEach(k => pool.push([k, all_choices[k]]));

    if (player_count * selection_count > pool.length) {
        return null;
    }

    var results = [];
    for (var p = 0; p < player_count; ++p) {
        var civs = [];
        for (var s = 0; s < selection_count; ++s) {
            if (pool.length == 0) {
                return null;
            }
            var idx = Math.floor(Math.random() * pool.length);
            var choice = pool[idx];
            var leader = choice[0];
            var nations = choice[1];
            var nation_idx = Math.floor(Math.random() * nations.length);
            var nation = nations[nation_idx];
            pool = pool.filter(c => c[0] != leader && c[1] != nation);
            civs.push({ leader: leader, nation: nation });
        }
        results.push({ player: players[p], civs: civs });
    }
    return results;
}

module.exports = {
    draftGS: (nCivs, players) => {
        var total = players.length * nCivs;
        if (total > all_nations.size || total > all_leaders.size) {
            return null;
        }

        while (true) {
            results = draft_gs_round(nCivs, players);
            if (results !== null) {
                return results;
            }
        }
    },
}
