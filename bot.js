const Discord = require('discord.js');
const logger = require('winston');
const auth = require('./auth.json');
const civ6 = require('./civ6.js');

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

const client = new Discord.Client();

client.on('ready', () => {
    logger.info('Connected');
});

function renderMarkdown(draft) {
    var ret = [];
    draft.forEach(player => {
        var civs = player.civs.map(civ => {
            var sigil = ":globe_with_meridians:";
            return sigil + " " + civ.leader + " (" + civ.nation + ")";
        });
        ret.push(player.player + ": " + civs.join(", "));
    });
    return ret.join("\n");
}

client.on('message', message => {
    if (message.author.bot) return;

    if (message.content.substring(0, 1) == '!') {
        var args = message.content.substring(1).split(' ');
        var cmd = args[0];

        args = args.splice(1);
        switch (cmd) {
            case 'draft': {
                if (args.length < 1) break;
                var nCivs = Number.parseInt(args[0], 10);
                if (Number.isNaN(nCivs)) {
                    nCivs = 3;
                    players = args;
                }
                else {
                    players = args.splice(1);
                }
                if (players.length) {
                    logger.info('Drafting ' + nCivs + ' civs for players ' + players);
                    var results = civ6.draftGS(nCivs, players);
                    if (results) {
                        message.channel.send(renderMarkdown(results));
                    }
                    else {
                        message.channel.send("Couldn't draft, probably too many civs/players requested.");
                    }
                }
            } break;
        }
    }
});

client.login(auth.token);
